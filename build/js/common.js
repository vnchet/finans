
$(window).on('load', function () {
    var $preloader = $('#page-preloader'),
        $spinner   = $preloader.find('.spinner');
    $spinner.fadeOut();
    $preloader.delay(350).fadeOut('slow');
});

$(document).ready(function() {
    $('.slider').slick({
        infinite: true,
        slidesToShow: 1,
        slidesToScroll: 1,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        responsive: [
            {
                breakpoint: 576,
                settings: {
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    dots: false,
                    arrows: false
                }
            },
        ]
    });
});

$(document).ready(function(){
    $('.go_to').click( function(){ // ловим клик по ссылке с классом go_to
        $('.menu_btn').removeClass('active');
        $('.menu_active').slideUp("slow");
        $('.bg').fadeOut("slow");
    });
});


$('.menu_btn').click(function() {
    if ($('.menu_active').is(':visible')) {
        $('.menu_btn').removeClass('active');
        $('.menu_active').slideUp("slow");
        $('.bg').fadeOut("slow");
    }
    else {
        $('.menu_btn').addClass('active');
        $('.menu_active').slideDown("slow");
        $('.bg').fadeIn("slow");
    }
});


/* IOS Bug dont Use this code */
/* -------------------------------------------------------------------------
   begin Video Youtube
 * ------------------------------------------------------------------------- */
//youtube script
var tag = document.createElement('script');
tag.src = "//www.youtube.com/iframe_api";
var firstScriptTag = document.getElementsByTagName('script')[0];
firstScriptTag.parentNode.insertBefore(tag, firstScriptTag);

var player;

onYouTubeIframeAPIReady = function () {
    player = new YT.Player('player', {
        height: '100%',
        width: '100%',
        videoId: 'hCyfnNQQWi8',  // youtube video id
        playerVars: {
            'autoplay': 1,
            'rel': 0,
            'showinfo': 0
        },
        events: {
            'onStateChange': onPlayerStateChange
        }
    });
};

var p = document.getElementById ("player");
$(p).hide();

var t = document.getElementById ("thumbnail");
t.src = "./build/img/imgVideo.png";

onPlayerStateChange = function (event) {
    if (event.data == YT.PlayerState.ENDED) {
        $('.start-video').fadeIn('normal');
    }
};

$(document).on('click', '.start-video', function () {
    $(this).hide();
    $("#player").show();
    $("#thumbnail_container").hide();
    player.playVideo();
});

/* -------------------------------------------------------------------------
   end Video Youtube
 * ------------------------------------------------------------------------- */


/*calculator*/
var deposit = 1000000;
var months = 24;
var percent = 0.1395;
var result;
var sum;
var resultDischarge;
var resultRounding;
var sumDischarge;
var sumRounding;
var inputDischarge;


function calculate() {
    result = deposit * percent * (months/12);//Формула дохода
    sum = deposit + result;//Формула итоговой суммы

    resultRounding = Math.round(result) + "₽";//Округление дохода
    sumRounding = Math.round(sum) + "₽";//Округление итоговой суммы

    resultDischarge = (resultRounding + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');//Приведение дохода к разряду
    sumDischarge = (sumRounding + '').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 ');//Приведение итоговой суммы к разряду

    $("#incomeNumber").text(resultDischarge);//Селектор, куда рендерится доход
    $("#sum").text(sumDischarge);//Селектор, куда рендерится итоговая сумма

}
calculate();

$("#polzunok").slider({
    animate: "slow",
    range: "min",
    value: deposit,
    min : 10000,//Минимально возможное значение на ползунке
    max : 5000000,//Максимально возможное значение на ползунке
    step : 5000,//Шаг, с которым будет двигаться ползунок
    slide : function(event, ui) {
        $("#result-polzunok").val((ui.value + '₽').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));
        deposit = ui.value;
        calculate();
    }

});

inputDischarge = $( "#polzunok" ).slider( "value" );
$( "#result-polzunok" ).val((inputDischarge + '₽').replace(/(\d)(?=(\d\d\d)+([^\d]|$))/g, '$1 '));


$("#polzunok2").slider({

    animate: "slow",
    range: "min",
    value: months,
    min : 12,//Минимально возможное значение на ползунке
    max : 84,//Максимально возможное значение на ползунке
    step : 12,//Шаг, с которым будет двигаться ползунок
    slide : function(event, ui) {
        $("#result-polzunok2").val(ui.value + ' месяцa(ев)');
        months = ui.value;
        calculate();
    }
});
$( "#result-polzunok2" ).val(($( "#polzunok2" ).slider( "value" )) + ' месяцa(ев)');

/*$("#percent").change(function (e) {
    percent = e.target.value;
    calculate();
});*/

/*//calculator*/


$(document).ready(function(){
    $('.go_to').click( function(){ // ловим клик по ссылке с классом go_to
        var scroll_el = $(this).attr('href'); // возьмем содержимое атрибута href, должен быть селектором, т.е. например начинаться с # или .
        if ($(scroll_el).length != 0) { // проверим существование элемента чтобы избежать ошибки
            $('html, body').animate({ scrollTop: $(scroll_el).offset().top }, 500); // анимируем скроолинг к элементу scroll_el
        }
        return false; // выключаем стандартное действие
    });
});


$(document).ready(function(){
    $('#button_contacts').click(function(){
        var form_name   = $('#form_name').val();
        var form_email   = $('#form_email').val();
        $.ajax({
            url: "post.php",
            type: "post",
            dataType: "json",
            data: {
                "form_name":   form_name,
                "form_email":   form_email,
            },
            success: function(data){
                $('.messages').html(data.result);
                $('#modal').hide('slow');
            }
        });
    });
});


