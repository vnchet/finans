const gulp = require('gulp');
const concat = require('gulp-concat');
const sass = require('gulp-sass');
const autoprefixer = require('gulp-autoprefixer');
const del = require('del');
const browserSync = require('browser-sync').create();

sass.compiler = require('node-sass');

const scssFiles = [
    './src/sass/main.scss',
    './src/sass/media.scss'
];

function styles() {
    return gulp.src(scssFiles)
        .pipe(sass({
            // includePaths: require('node-normalize-scss').with('other/path', 'another/path')
            // - or -
            includePaths: require('node-normalize-scss').includePaths
        }))
        .pipe(sass().on('error', sass.logError))
        .pipe(concat('all.css'))
        .pipe(autoprefixer({
            browsers: ['> 0.1%'],
            cascade: false
        }))
        .pipe(gulp.dest('./build/css'))
        .pipe(browserSync.stream());
}
function scripts() {
    return gulp.src('./src/js/common.js')
        .pipe(concat('common.js'))
        .pipe(gulp.dest('./build/js'))
        .pipe(browserSync.stream());
}

function watch() {
    browserSync.init({
        server: {
            baseDir: "./"
        }
    });
    gulp.watch('./src/sass/**/*.scss', styles);
    gulp.watch('./src/js/**/*.js', scripts);
    gulp.watch('./*.html').on('change', browserSync.reload);
}

function clean() {
    return del(['build/*'])
}

gulp.task('styles', styles);
gulp.task('scripts', scripts);
gulp.task('watch', watch);
gulp.task('build', gulp.series(clean,
    gulp.parallel(styles,scripts)));